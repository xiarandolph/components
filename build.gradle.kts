plugins {
    id("org.gradle.maven-publish")
    id("org.jetbrains.kotlin.jvm").version("1.5.21")
    id("com.github.johnrengelman.shadow").version("7.0.0")
}

group = "com.gitlab.ballysta"
version = "1.0.10"

repositories {
    maven("https://jitpack.io"); mavenLocal()
    maven("https://repo1.maven.org/maven2")
}

dependencies {
    api("com.gitlab.ballysta:architecture:1.0.9")
    api("redis.clients:jedis:2.8.0")
    implementation("org.apache.commons:commons-pool2:2.9.0")
    compileOnly(files("./Server/Server.jar"))
}

publishing {
    publications {
        create<MavenPublication>(project.name) {
            artifactId = project.name.toLowerCase()
            from(components["java"])
        }
    }
}

tasks.shadowJar{
    destinationDirectory.set(file("./Server/plugins"))
    archiveFileName.set("TestPlugin.jar")

}
tasks.compileKotlin {
    kotlinOptions.freeCompilerArgs += "-Xopt-in=kotlin.time.ExperimentalTime"
}

package com.gitlab.ballysta.components.redis

import com.gitlab.ballysta.architecture.*
import redis.clients.jedis.BinaryJedis
import redis.clients.jedis.BinaryJedisPubSub
import redis.clients.jedis.JedisPool
import java.nio.ByteBuffer.allocate
import java.nio.ByteBuffer.wrap
import java.util.*
import java.util.concurrent.CompletableFuture
import java.util.concurrent.Executors
import java.util.concurrent.Future
import kotlin.text.Charsets.UTF_8

typealias Redis = () -> (BinaryJedis)

val Redis.table get(): Toggled.(Any) -> (Mutable<ByteArray, ByteArray>) = { map ->
    val name = map.toString().toByteArray(UTF_8)
    val channel = "$map:onChanged".toByteArray(UTF_8)
    object : BinaryJedisPubSub(), Mutable<ByteArray, ByteArray> {
        override val onChanged = TreeEvent<(ByteArray, ByteArray?, ByteArray?) -> Unit>()
        override fun get(key: ByteArray) = invoke().use { it.hget(name, key) }
        override fun set(key: ByteArray, value: ByteArray?) = invoke().use {
            val result = it.multi().apply {
                hget(name, key)
                if (value == null) hdel(name, key)
                else hset(name, key, value)
            }.exec()
            (result[0] as ByteArray?).also { from ->
                if ((result[1] as Long) == 1L) {
                    val buffer = allocate(8 + key.size + (from?.size ?: 0) + (value?.size ?: 0))
                    buffer.putInt(key.size).put(key).putInt(from?.size ?: 0)
                    if (from != null) buffer.put(from)
                    if (value != null) buffer.put(value)
                    //TODO find a way to wait until all listeners have been called.
                    it.publish(channel, buffer.array())
                }
            }
        }

        override val size = invoke().use { it.hlen(name).toInt() }
        //TODO replace these with scan at some point. https://srcb.in/cfFI64dS7J
        //TODO consider the issue of atomic iteration (requires framework redesign?)
        override val keys get() = invoke().use { it.hkeys(name).enumerator() }
        override val values get() = invoke().use { it.hvals(name).enumerator() }

        val executor = Executors.newSingleThreadExecutor()
        var task: Future<*>? = null
        var future: CompletableFuture<Unit>? = null
        override fun onMessage(channel: ByteArray, message: ByteArray) {
            val buffer = wrap(message)
            val key = ByteArray(buffer.int).also(buffer::get)
            val length = buffer.int
            val from = if (length < 1) null else
                ByteArray(length).also(buffer::get)
            val to = if (!buffer.hasRemaining()) null else
                ByteArray(buffer.remaining()).also(buffer::get)
            onChanged(key, from, to)
        }
        override fun onSubscribe(channel: ByteArray, count: Int) { future?.complete(Unit) }
        init {
            onEnabled {
                future = CompletableFuture<Unit>()
                task = executor.submit { invoke().use {
                    it.subscribe(this, channel); it.quit()
                } }
                future?.get()
            }
            onDisabled {
                if (isSubscribed) unsubscribe()
                task?.cancel(true)
                task = null; future = null
            }
        }
    }
}

fun <Value> Mutable<ByteArray, Value>.stringKeys() =
    mapKeys({ it.toString(UTF_8) }, { it.toByteArray(UTF_8) })
fun <Key> Mutable<Key, ByteArray>.stringValues() =
    mapValues({ it.toString(UTF_8) }, { it.toByteArray(UTF_8) })

fun <Value> Mutable<ByteArray, Value>.uuidKeys() = mapKeys(
    { wrap(it).let { buffer -> UUID(buffer.long, buffer.long) } },
    { allocate(16).putLong(it.mostSignificantBits).putLong(it.leastSignificantBits).array() }
)
fun <Key> Mutable<Key, ByteArray>.uuidValues() = mapValues(
    { wrap(it).let { buffer -> UUID(buffer.long, buffer.long) } },
    { allocate(16).putLong(it.mostSignificantBits).putLong(it.leastSignificantBits).array() }
)

fun <Value> Mutable<ByteArray, Value>.intKeys() = mapKeys({
    (it[0].toInt() shl 0x00) or (it[1].toInt() shl 0x08) or
    (it[2].toInt() shl 0x10) or (it[3].toInt() shl 0x18)
}, { byteArrayOf(
    (it shr 0x00 and 0xFF).toByte(), (it shr 0x08 and 0xFF).toByte(),
    (it shr 0x10 and 0xFF).toByte(), (it shr 0x18 and 0xFF).toByte()
) })
fun <Key> Mutable<Key, ByteArray>.intValues() = mapValues({
    (it[0].toInt() shl 0x00) or (it[1].toInt() shl 0x08) or
    (it[2].toInt() shl 0x10) or (it[3].toInt() shl 0x18)
}, { byteArrayOf(
    (it shr 0x00 and 0xFF).toByte(), (it shr 0x08 and 0xFF).toByte(),
    (it shr 0x10 and 0xFF).toByte(), (it shr 0x18 and 0xFF).toByte()
) })

fun <Value> Mutable<ByteArray, Value>.booleanKeys() =
    mapKeys({ it[0] > 0 }, { byteArrayOf(if (it) 0 else 1) })
fun <Key> Mutable<Key, ByteArray>.booleanValues() =
    mapValues({ it[0] > 0 }, { byteArrayOf(if (it) 0 else 1) })
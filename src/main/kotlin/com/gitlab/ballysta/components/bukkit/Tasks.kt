@file:Suppress("NOTHING_TO_INLINE")
package com.gitlab.ballysta.components.bukkit

import com.gitlab.ballysta.architecture.*
import org.bukkit.Bukkit.*
import org.bukkit.scheduler.BukkitTask
import kotlin.time.Duration
import kotlin.time.milliseconds

private val STUB_PLUGIN = StubPlugin("SuzumebachiTasks")

val Int.ticks get() = times(50).milliseconds
val Long.ticks get() = times(50L).milliseconds
val Duration.inTicks get() = inMilliseconds / 50
fun Duration.toLongTicks() = toLongMilliseconds() / 50L

val tick = 1.ticks

fun every(interval: Duration): Observable<Int> = {
    var ticks = 0
    var task: BukkitTask? = null
    onEnabled {
        task = getScheduler().runTaskTimer(STUB_PLUGIN, Runnable {
            it(ticks++)
        }, interval.toLongTicks(), interval.toLongTicks())
    }
    onDisabled { task?.cancel(); task = null; ticks = 0 }
}
@Deprecated(
    "I no longer want to return boolean here.",
    replaceWith = ReplaceWith("every(period)(){}"
)) fun Toggled.every(
    interval: Duration, task: (Int) -> (Boolean)
) {
    val component = Component {
        every(interval)() { if (!task(it)) disable() }
    }
    onDisabled(component::disable)
    onEnabled(component::enable)
}

fun after(interval: Duration): Event<() -> (Unit)> = {
    var task: BukkitTask? = null
    onEnabled {
        task = getScheduler().runTaskLater(STUB_PLUGIN, Runnable {
            if (task != null) it()
        }, interval.toLongTicks())
    }; onDisabled { task?.cancel(); task = null }
}
inline fun Toggled.after(
    interval: Duration, noinline task: () -> (Unit)
) = after(interval)(task)





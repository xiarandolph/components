package com.gitlab.ballysta.components.bukkit

import com.gitlab.ballysta.architecture.ComponentEvent
import com.gitlab.ballysta.architecture.Toggled
import com.gitlab.ballysta.architecture.invoke
import org.bukkit.plugin.java.JavaPlugin

open class ComponentPlugin(block: ComponentPlugin.() -> (Unit)) : JavaPlugin(), Toggled {
    private val enabledEvent = ComponentEvent()
    private val disabledEvent = ComponentEvent()

    final override val onEnabled = enabledEvent
    final override val onDisabled = disabledEvent
    final override val enabled get() = isEnabled

    final override fun onEnable()  = enabledEvent()
    final override fun onDisable() = disabledEvent()

    init { block() }
}
@file:Suppress("UNCHECKED_CAST")
package com.gitlab.ballysta.components

import com.gitlab.ballysta.architecture.Event
import com.gitlab.ballysta.architecture.TreeEvent
import io.netty.buffer.ByteBuf
import io.netty.channel.Channel
import io.netty.channel.ChannelDuplexHandler
import io.netty.channel.ChannelHandlerContext
import io.netty.channel.ChannelPromise
import org.bukkit.Bukkit
import org.bukkit.entity.Player
import java.lang.Class.forName
import java.util.*
import java.util.function.Function

private val INJECTORS = IdentityHashMap<Player, Handler>()
private val PATH = Bukkit.getServer().javaClass.`package`.name
private val HANDLE = forName("$PATH.entity.CraftPlayer").getMethod("getHandle")
private val CONNECTION = HANDLE.returnType.getDeclaredField("playerConnection")
private val MANAGER = CONNECTION.type.getDeclaredField("networkManager")
private val FACTORY = Function<Player, Handler?> {
    val channel = MANAGER[CONNECTION[HANDLE(it)]]
    NETWORK[channel]?.run { Handler(it, this as Channel) }
}
private val NETWORK = MANAGER.type.declaredFields.find {
    it.isAccessible = true; it.type == Channel::class.java
}!!

private class Handler(val player: Player, val channel: Channel) : ChannelDuplexHandler() {
    val listeners = Array<TreeEvent<(ByteBuf) -> (Boolean)>>(256) { TreeEvent() }
    val encoder = channel.pipeline().context("encoder")!!
    init { channel.pipeline().addBefore("decoder", "custom_packet_handler", this) }

    override fun channelRead(context: ChannelHandlerContext, msg: Any) {
        val buffer = (msg as ByteBuf).duplicate()
        if (buffer.isReadable) {
            val id = buffer.readUnsignedByte()
            buffer.markReaderIndex()
            listeners[id.toInt()].forEach {
                if (!it(buffer.resetReaderIndex())) return
            }
        }
        super.channelRead(context, msg)
    }
    override fun write(context: ChannelHandlerContext, msg: Any, promise: ChannelPromise) {
        val buffer = (msg as ByteBuf).duplicate()
        if (buffer.isReadable) {
            val id = buffer.readUnsignedByte()
            buffer.markReaderIndex()
            listeners[id.toInt()].forEach {
                if (!it(buffer.resetReaderIndex())) return
            }
        }
        super.write(context, msg, promise)
    }
    override fun channelUnregistered(ctx: ChannelHandlerContext) {
        super.channelUnregistered(ctx); INJECTORS.remove(player)
    }
}

/*
fun <Type : Any> Player.onHandle(type: KClass<Type>): Event<Type.() -> (Boolean)> = {
    val protocol = (PROTOCOL.get(null) as Map<*, *>)[type.java]
    val direction = DIRECTION.get(protocol) as Map<*, *>
    val id = ID(direction[IN], type.java) ?: ID(direction[OUT], type.java)
    val packet = type.java.getConstructor().newInstance()
    onHandle(id as Int)() {
//        val serializer = PACKET_DATA_SERIALIZER_CONSTRUCTOR.newInstance(this)
        try {
            (packet as Packet<*>).a(PacketDataSerializer(this))
//            PACKET_READ(packet, serializer)
        } catch (exception: Exception) {
            println("Error reading")
            exception.printStackTrace()
        }
        println(packet)
        it(packet)
    }
}
*/
//inline fun <reified Type : Any> Player.onHandle() = onHandle(Type::class)

fun Player.onHandle(id: Int): Event<ByteBuf.() -> (Boolean)> = {
    val handler = INJECTORS.computeIfAbsent(this@onHandle, FACTORY)
    handler.listeners[id](it)
}

fun Player.handle(packet: Any) {
    val handler = INJECTORS.computeIfAbsent(this, FACTORY)
    handler?.channel?.write(packet)
}
fun Player.handle(id: Int, packet: ByteBuf.() -> (Unit)) {
    val handler = INJECTORS.computeIfAbsent(this, FACTORY)
    (handler ?: return).encoder.alloc().buffer().apply {
        writeByte(id); packet()
        handler.encoder.writeAndFlush(this)
    }
}